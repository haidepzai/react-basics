import { useState } from 'react'
import Todo from './Todo'

//Problem Memory Leak
//In Todo werden die Todos gefetch (dauert 3 Sek)
//Wenn mann dann Toggle macht hier, dann ist die Todo weg
//Aber der Request läuft noch...

function UseRefExample3() {
  const [showTodo, setShowTodo] = useState(true)

  return (
    <div>
      {showTodo && <Todo />}
      <button
        className='btn btn-primary'
        onClick={() => setShowTodo(!showTodo)}
      >
        Toggle Todo
      </button>
    </div>
  )
}

export default UseRefExample3
